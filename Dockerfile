###### Install dependencies only when needed ######
FROM node:lts-alpine AS builder

# Make /app as working directory
WORKDIR /app

# Copy package.json file
COPY package.json .
COPY package-lock.json .

# Install dependencies
RUN npm install

# Copy the source code to the /app directory
COPY . .

# Build the application
RUN npm run build


######  Use NgInx alpine image  ###### 
FROM nginx:stable-alpine

# Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# Copy nginx config file
COPY ./misc/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./misc/nginx/security-headers.conf /etc/nginx/security-headers.conf

# Copy dist folder fro build stage to nginx public folder
COPY --from=builder /app/dist/zen-ui /usr/share/nginx/html

# Start NgInx service
ENTRYPOINT ["nginx"]

