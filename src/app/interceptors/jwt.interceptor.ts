import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';
import { JwtService } from '@services/jwt.service';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(
    private readonly apiService: ApiService,
    private readonly jwtService: JwtService
  ) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (this.apiService.isApiUrl(request.url)) {
      const jwt = this.jwtService.getToken();

      if (jwt) {
        return next.handle(
          request.clone({
            setHeaders: { Authorization: `Bearer ${jwt}` },
          })
        );
      }
    }

    return next.handle(request);
  }
}
