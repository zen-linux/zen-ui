import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { ApiKey } from '@entities/api-key';
import { ApiKeysService } from '@services/api-keys.service';
import { BehaviorSubject, Observable, switchMap } from 'rxjs';

@Component({
  selector: 'app-api-keys',
  templateUrl: './api-keys.component.html',
  styleUrls: ['./api-keys.component.scss'],
  standalone: true,
  imports: [CommonModule, ClarityModule, FormsModule],
})
export class ApiKeysComponent implements OnInit {
  apiKeys$: Observable<ApiKey[]>;

  newApiKeyDescription = '';

  private readonly reloadSubject = new BehaviorSubject<void>(undefined);

  constructor(private apiKeysService: ApiKeysService) {
    this.apiKeys$ = this.reloadSubject
      .asObservable()
      .pipe(switchMap(() => this.apiKeysService.getApiKeys()));
  }

  ngOnInit(): void {
    console.log('onInit');

    this.triggerLoad();
  }

  createApiKey() {
    this.apiKeysService
      .createApiKey(this.newApiKeyDescription)
      .subscribe((apiKey) => {
        console.log('created new api key:', apiKey);
        this.triggerLoad();
      });
  }

  deleteApiKey(apiKey: ApiKey) {
    this.apiKeysService.deleteApiKey(apiKey).subscribe(() => {
      console.log('api key deleted');
      this.triggerLoad();
    });
  }

  private triggerLoad() {
    console.log('trigger reload');
    this.reloadSubject.next();
  }
}
