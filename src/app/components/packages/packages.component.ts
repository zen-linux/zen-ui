import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import { Pkg } from '@entities/pkg';
import { PkgsService } from '@services/pkgs.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss'],
  standalone: true,
  imports: [CommonModule, ClarityModule],
})
export class PackagesComponent {
  pkgs$: Observable<Pkg[]>;

  constructor(private pkgsService: PkgsService) {
    this.pkgs$ = pkgsService.getAllPkgs();
  }
}
