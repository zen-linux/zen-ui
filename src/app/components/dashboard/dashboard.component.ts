import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { User } from '@entities/user';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  standalone: true,
  imports: [CommonModule],
})
export class DashboardComponent {
  user$: Observable<User | null>;

  constructor(private alertService: AlertService, authService: AuthService) {
    this.user$ = authService.user$;
  }

  addErrorAlert() {
    this.alertService.error('This is just a test error');
  }

  addWarningAlert() {
    this.alertService.warning('This is just a test warning');
  }

  addInfoAlert() {
    this.alertService.info('This is just a test info');
  }

  clearAlerts() {
    this.alertService.clear();
  }
}
