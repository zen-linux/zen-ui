import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { User } from '@entities/user';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  standalone: true,
  imports: [CommonModule],
})
export class UserProfileComponent {
  user$: Observable<User | null>;

  constructor(readonly authService: AuthService) {
    this.user$ = authService.user$;
  }
}
