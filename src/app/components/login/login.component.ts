import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ClarityModule } from '@clr/angular';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  imports: [ClarityModule, FormsModule],
})
export class LoginComponent {
  email = '';
  password = '';
  remember = false;

  constructor(private authService: AuthService, private router: Router) {}

  submit() {
    console.log('submit');

    this.authService
      .login(this.email, this.password, this.remember)
      .subscribe((success) => {
        if (success) {
          this.router.navigate(['dashboard']);
        }
      });
  }
}
