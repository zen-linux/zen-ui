export interface ApiKey {
  id: string;
  key: string;
  description: string;
}
