export interface MetaInfo {
  createdAt: Date;
  updatedAt: Date;
}
