export class Alert {
  private constructor(
    public readonly type: 'danger' | 'warning' | 'info',
    public readonly text: string
  ) {}

  static Error(text: string): Alert {
    return new Alert('danger', text);
  }

  static Warning(text: string): Alert {
    return new Alert('warning', text);
  }

  static Info(text: string): Alert {
    return new Alert('info', text);
  }
}
