export interface Pkg {
  id: string;
  name: string;
  description: string;
  version: string;
  release: number;
  size: number;
}
