import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'dashboard',
    loadComponent: () =>
      import('./components/dashboard/dashboard.component').then(
        (mod) => mod.DashboardComponent
      ),
  },
  {
    path: 'downloads',
    loadComponent: () =>
      import('./components/downloads/downloads.component').then(
        (mod) => mod.DownloadsComponent
      ),
  },
  {
    path: 'packages',
    loadComponent: () =>
      import('./components/packages/packages.component').then(
        (mod) => mod.PackagesComponent
      ),
  },
  {
    path: 'repositories',
    loadComponent: () =>
      import('./components/repositories/repositories.component').then(
        (mod) => mod.RepositoriesComponent
      ),
  },
  {
    path: 'user/settings',
    loadComponent: () =>
      import('./components/user-profile/user-profile.component').then(
        (mod) => mod.UserProfileComponent
      ),
  },
  {
    path: 'user/api-keys',
    loadComponent: () =>
      import('./components/api-keys/api-keys.component').then(
        (mod) => mod.ApiKeysComponent
      ),
  },
  {
    path: 'login',
    loadComponent: () =>
      import('./components/login/login.component').then(
        (mod) => mod.LoginComponent
      ),
  },
  {
    path: 'register',
    loadComponent: () =>
      import('./components/register/register.component').then(
        (mod) => mod.RegisterComponent
      ),
  },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
