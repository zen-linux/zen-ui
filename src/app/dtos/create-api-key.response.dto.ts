export interface CreateApiKeyResponseDto {
  id: string;
  key: string;
  description: string;
}
