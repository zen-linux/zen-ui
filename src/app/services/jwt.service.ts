import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

const TOKEN_KEY = 'jwt';

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  private jwtSubject: BehaviorSubject<string | null>;

  jwt$: Observable<string | null>;

  constructor() {
    this.jwtSubject = new BehaviorSubject<string | null>(this.loadToken());
    this.jwt$ = this.jwtSubject.asObservable();
  }

  setToken(jwt: string, remember = false) {
    this.saveToken(jwt, remember);
    this.jwtSubject.next(jwt);
  }

  getToken(): string | null {
    return this.jwtSubject.value;
  }

  deleteToken(): void {
    this.jwtSubject.next(null);
    sessionStorage.removeItem(TOKEN_KEY);
    localStorage.removeItem(TOKEN_KEY);
  }

  private loadToken(): string | null {
    let jwt = sessionStorage.getItem(TOKEN_KEY);
    if (jwt === null) {
      jwt = localStorage.getItem(TOKEN_KEY);
    }
    console.log('loaded token:', jwt);
    return jwt;
  }

  private saveToken(jwt: string, remember = false) {
    console.log('saving token, remember:', remember);
    if (remember) {
      localStorage.setItem(TOKEN_KEY, jwt);
      sessionStorage.removeItem(TOKEN_KEY);
    } else {
      sessionStorage.setItem(TOKEN_KEY, jwt);
      localStorage.removeItem(TOKEN_KEY);
    }
  }
}
