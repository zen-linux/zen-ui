import { Injectable } from '@angular/core';
import { environment } from '@env';
import { AppConfig } from 'src/environments/app-config';

@Injectable({
  providedIn: 'root',
})
export class EnvService implements AppConfig {
  get production(): boolean {
    return environment.production;
  }

  get apiUrl(): string {
    return environment.apiUrl;
  }
}
