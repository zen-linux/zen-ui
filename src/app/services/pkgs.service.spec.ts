import { TestBed } from '@angular/core/testing';

import { PkgsService } from './pkgs.service';

describe('PkgsService', () => {
  let service: PkgsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PkgsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
