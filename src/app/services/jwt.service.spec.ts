import { TestBed } from '@angular/core/testing';
import { JwtService } from './jwt.service';

describe('JwtService', () => {
  let service: JwtService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JwtService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should correctly set and get a token', () => {
    const token = 'test-token';
    service.setToken(token);
    expect(service.getToken()).toEqual(token);
  });

  it('should save token to local storage when remember is set', () => {
    const sessionStorageSpy = spyOn(sessionStorage, 'setItem');
    const localStorageSpy = spyOn(localStorage, 'setItem');

    const token = 'test-token';
    service.setToken(token, true);

    expect(sessionStorageSpy).not.toHaveBeenCalled();
    expect(localStorageSpy).toHaveBeenCalled();
  });

  it('should save token to session storage when remember is not set', () => {
    const sessionStorageSpy = spyOn(sessionStorage, 'setItem');
    const localStorageSpy = spyOn(localStorage, 'setItem');

    const token = 'test-token';
    service.setToken(token, false);

    expect(sessionStorageSpy).toHaveBeenCalled();
    expect(localStorageSpy).not.toHaveBeenCalled();
  });
});
