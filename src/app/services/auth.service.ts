import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginRequestDto } from '@dtos/login-request.dto';
import { LoginResponseDto } from '@dtos/login-response.dto';
import { User } from '@entities/user';
import {
  BehaviorSubject,
  catchError,
  distinctUntilChanged,
  map,
  Observable,
  of,
  shareReplay,
  switchMap,
  tap,
  throwError,
} from 'rxjs';
import { AlertService } from './alert.service';
import { ApiService } from './api.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly userSubject = new BehaviorSubject<User | null>(null);

  user$: Observable<User | null>;

  constructor(
    private readonly apiService: ApiService,
    private readonly jwtService: JwtService,
    private readonly http: HttpClient,
    private readonly alertService: AlertService
  ) {
    this.user$ = this.createUserObservable();
  }

  login(
    username: string,
    password: string,
    remember = false
  ): Observable<boolean> {
    return this.loginRequest(username, password).pipe(
      tap((result) => {
        this.jwtService.setToken(result.accessToken, remember);
      }),
      map(() => true),
      catchError((error) => {
        if (error instanceof HttpErrorResponse && error.status === 401) {
          return of(false);
        }
        return throwError(() => error);
      })
    );
  }

  logout() {
    this.jwtService.deleteToken();
    this.alertService.info('You are no longer logged in');
  }

  private createUserObservable(): Observable<User | null> {
    return this.jwtService.jwt$.pipe(
      distinctUntilChanged(),
      switchMap((jwt) => {
        if (jwt === null) {
          return of(null);
        }
        return this.currentUserRequest();
      }),
      shareReplay(1)
    );
  }

  private currentUserRequest(): Observable<User> {
    const url = this.apiService.buildUrl('v1/users/self');
    return this.http.get<User>(url);
  }

  private loginRequest(
    email: string,
    password: string
  ): Observable<LoginResponseDto> {
    const url = this.apiService.buildUrl('v1/auth/login');
    const body: LoginRequestDto = {
      email,
      password,
    };
    return this.http.post<LoginResponseDto>(url, body);
  }
}
