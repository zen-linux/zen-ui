import { TestBed } from '@angular/core/testing';
import { ApiService } from './api.service';
import { EnvService } from './env.service';

describe('ApiService', () => {
  let service: ApiService;
  let envService: EnvService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiService);
    envService = TestBed.inject(EnvService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should remove trailing slash from apiUrl', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.com/'
    );
    expect(service.apiUrl()).toEqual('http://myapi.example.com');
  });

  it('should leave apiUrl as it is', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.net'
    );
    expect(service.apiUrl()).toEqual('http://myapi.example.net');
  });

  it('should build url with additional slash', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.com/'
    );
    expect(service.buildUrl('users')).toEqual('http://myapi.example.com/users');
  });

  it('should build url without additional slash', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.com/'
    );
    expect(service.buildUrl('/users')).toEqual(
      'http://myapi.example.com/users'
    );
  });

  it('should detect url correctly as api url', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.com/'
    );
    expect(service.isApiUrl('http://myapi.example.com')).toBeTrue();
    expect(service.isApiUrl('http://myapi.example.com/')).toBeTrue();
    expect(service.isApiUrl('http://myapi.example.com/auth/login')).toBeTrue();
  });

  it('should detect url correctly not as api url', () => {
    spyOnProperty(envService, 'apiUrl').and.returnValue(
      'http://myapi.example.com/'
    );
    expect(service.isApiUrl('')).toBeFalse();
    expect(service.isApiUrl('/')).toBeFalse();
    expect(service.isApiUrl('/api')).toBeFalse();
    expect(service.isApiUrl('http://myapi.example.ch')).toBeFalse();
    expect(service.isApiUrl('http://myapi.example.ch/auth/login')).toBeFalse();
  });
});
