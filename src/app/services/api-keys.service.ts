import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateApiKeyRequestDto } from '@dtos/create-api-key.request.dto';
import { CreateApiKeyResponseDto } from '@dtos/create-api-key.response.dto';
import { ApiKey } from '@entities/api-key';
import { BehaviorSubject, map, Observable, Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class ApiKeysService {
  private apiKeysSubject = new BehaviorSubject<ApiKey[]>([]);

  private readonly reloadSubject = new Subject<void>();

  apiKeys$: Observable<ApiKey[]>;

  constructor(
    private readonly apiService: ApiService,
    private readonly http: HttpClient
  ) {
    this.apiKeys$ = this.apiKeysSubject.asObservable();
  }

  getApiKeys(): Observable<ApiKey[]> {
    const url = this.apiService.buildUrl('v1/api-keys');
    return this.http
      .get<{ apiKeys: ApiKey[] }>(url)
      .pipe(map((body) => body.apiKeys));
  }

  createApiKey(description: string): Observable<ApiKey> {
    const url = this.apiService.buildUrl('v1/api-keys');
    const body: CreateApiKeyRequestDto = {
      description,
    };
    return this.http.post<CreateApiKeyResponseDto>(url, body).pipe(
      map((body) => ({
        id: body.id,
        key: body.key,
        description: body.description,
      }))
    );
  }

  deleteApiKey(apiKey: ApiKey): Observable<void> {
    const url = `${this.apiService.buildUrl('v1/api-keys')}/${apiKey.id}`;
    return this.http.delete(url).pipe(map(() => {}));
  }

  reload() {
    this.reloadSubject.next();
  }
}
