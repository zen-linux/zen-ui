import { Injectable } from '@angular/core';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private readonly environmentService: EnvService) {}

  apiUrl(): string {
    return this.environmentService.apiUrl.endsWith('/')
      ? this.environmentService.apiUrl.slice(0, -1)
      : this.environmentService.apiUrl;
  }

  buildUrl(path: string): string {
    return path.startsWith('/')
      ? `${this.apiUrl()}${path}`
      : `${this.apiUrl()}/${path}`;
  }

  isApiUrl(url: string): boolean {
    return url.startsWith(this.apiUrl());
  }
}
