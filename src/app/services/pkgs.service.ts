import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pkg } from '@entities/pkg';
import { BehaviorSubject, map, Observable, Subject } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class PkgsService {
  private pkgsSubject = new BehaviorSubject<Pkg[]>([]);

  private readonly reloadSubject = new Subject<void>();

  pkgs$: Observable<Pkg[]>;

  constructor(
    private readonly apiService: ApiService,
    private readonly http: HttpClient
  ) {
    this.pkgs$ = this.pkgsSubject.asObservable();
  }

  getAllPkgs(): Observable<Pkg[]> {
    const url = this.apiService.buildUrl('v1/pkgs');
    return this.http.get<{ pkgs: Pkg[] }>(url).pipe(map((body) => body.pkgs));
  }

  reload() {
    this.reloadSubject.next();
  }
}
