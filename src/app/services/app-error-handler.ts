import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable } from '@angular/core';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root',
})
export class AppErrorHandler implements ErrorHandler {
  constructor(private alertService: AlertService) {}

  handleError(error: any): void {
    //console.log('handleError:', error);

    if (error instanceof HttpErrorResponse) {
      this.alertService.error(`Unhandled http error: ${error.message}`);
      console.log(error.error);
    } else {
      this.alertService.error(`Unhandled error: ${error.message}`);
      console.error(error);
    }
  }
}
