import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Group } from '@entities/group';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class GroupsService {
  private selectedGroupSubject = new BehaviorSubject<Group | null>(null);

  readonly selectedGroup$ = this.selectedGroupSubject.asObservable();

  constructor(
    private readonly apiService: ApiService,
    private readonly http: HttpClient
  ) {}

  getGroups(): Observable<Group[]> {
    const url = this.apiService.buildUrl('groups');
    return this.http.get<Group[]>(url);
  }

  getSelectedGroup(): Group | null {
    return this.selectedGroupSubject.value;
  }

  setSelectedGroup(project: Group) {
    this.selectedGroupSubject.next(project);
  }
}
