import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Alert } from '../entities/alert';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  private alertsSubject = new BehaviorSubject<Alert[]>([]);

  alerts$: Observable<Alert[]>;

  constructor() {
    this.alerts$ = this.alertsSubject.asObservable();
  }

  error(text: string) {
    const alerts = this.alertsSubject.getValue();
    alerts.push(Alert.Error(text));
    this.alertsSubject.next(alerts);
  }

  warning(text: string) {
    const alerts = this.alertsSubject.getValue();
    alerts.push(Alert.Warning(text));
    this.alertsSubject.next(alerts);
  }

  info(text: string) {
    const alerts = this.alertsSubject.getValue();
    alerts.push(Alert.Info(text));
    this.alertsSubject.next(alerts);
  }

  clear() {
    this.alertsSubject.next([]);
  }
}
