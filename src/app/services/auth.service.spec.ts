import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { User } from '@app/models/user';
import { ApiService } from './api.service';
import { AuthService } from './auth.service';
import { JwtService } from './jwt.service';

describe('AuthService', () => {
  let service: AuthService;
  let apiService: ApiService;
  let jwtService: JwtService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
    service = TestBed.inject(AuthService);
    apiService = TestBed.inject(ApiService);
    jwtService = TestBed.inject(JwtService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('user observable', () => {
    it('should return null when no jwt exists', (done) => {
      spyOn(apiService, 'apiUrl').and.returnValue('http://api.example.com');
      spyOn(jwtService, 'getToken').and.returnValue(null);

      service.user$.subscribe({
        next: (user) => {
          expect(user).toBeNull();
          done();
        },
        error: () => {
          fail('should not end up here');
          done();
        },
      });
    });

    it('should return a user when a valid jwt exists', (done) => {
      const validUser: User = {
        username: 'ValidUser',
        email: '',
        id: '',
        active: true,
        metaInfo: { createdAt: new Date(), updatedAt: new Date() },
      };

      spyOn(apiService, 'apiUrl').and.returnValue('http://api.example.com');
      //spyOn(jwtService, 'getToken').and.returnValue('valid-jwt');

      service.user$.subscribe({
        next: (user) => {
          expect(user).toEqual(validUser);
          done();
        },
        error: () => {
          fail('should not end up here');
          done();
        },
      });

      const authReq = httpTestingController.expectOne(
        'http://api.example.com/users/current'
      );
      authReq.flush(validUser);

      jwtService.setToken('valid-token');

      httpTestingController.verify();
    });
  });
});
