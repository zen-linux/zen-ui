import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  bundleIcon,
  ClarityIcons,
  cogIcon,
  dashboardIcon,
  downloadCloudIcon,
  storageIcon,
} from '@cds/core/icon';
import { User } from '@entities/user';
import { AuthService } from '@services/auth.service';
import { Observable } from 'rxjs';
import { Alert } from './entities/alert';
import { AlertService } from './services/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  alerts$: Observable<Alert[]>;
  user$: Observable<User | null>;

  constructor(
    alertService: AlertService,
    private authService: AuthService,
    private readonly router: Router
  ) {
    this.alerts$ = alertService.alerts$;
    this.user$ = authService.user$;
  }

  ngOnInit(): void {
    ClarityIcons.addIcons(
      bundleIcon,
      cogIcon,
      dashboardIcon,
      downloadCloudIcon,
      storageIcon
    );
  }

  logOut() {
    this.authService.logout();
    this.router.navigate(['dashboard']);
  }
}
