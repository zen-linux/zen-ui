import { AppConfig } from './app-config';

export const environment: AppConfig = {
  production: true,

  apiUrl: 'https://api.zenlinux.net',
};
